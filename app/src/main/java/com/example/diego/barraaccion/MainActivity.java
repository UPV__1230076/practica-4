package com.example.diego.barraaccion;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec spec1 = tabHost.newTabSpec("Conversor 1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Grados");

        TabHost.TabSpec spec2 = tabHost.newTabSpec("Conversor 2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("Área");

        TabHost.TabSpec spec3 = tabHost.newTabSpec("Conversor 3");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Distancia");

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);

        final Button button = (Button) findViewById(R.id.btnconvertirFC);
        final EditText gf = (EditText) findViewById(R.id.gradosF);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                int gft = Integer.parseInt(gf.getText().toString());
                int t;
                TextView res1 = (TextView) findViewById(R.id.res1);
                t = ((gft - 32) * 5) / 9;
                res1.setText("Grados Celsius: " + t);

            }
        });

        final Button button2 = (Button) findViewById(R.id.btnArea);
        final EditText area = (EditText) findViewById(R.id.area);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                int a = Integer.parseInt(area.getText().toString());
                int ta = a * a;
                TextView res2 = (TextView) findViewById(R.id.res2);
                res2.setText("Área del cuadrado: " + ta);

            }
        });

        final Button button3 = (Button) findViewById(R.id.btnconvertirMK);
        final EditText millas = (EditText) findViewById(R.id.millas);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                double m = Double.parseDouble(millas.getText().toString());
                double k = m * 1.609;
                TextView res3 = (TextView) findViewById(R.id.res3);
                res3.setText("Kilómetros: " + k);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}